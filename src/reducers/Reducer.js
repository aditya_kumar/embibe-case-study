import {INIT_ACTION} from '../actions/Action';

export function studentReducer(state=[],action){

    switch(action.type){
        case INIT_ACTION:
            return {
                ...state,
                studentData: action.payload.data
            };
        default: return state;
    }
}