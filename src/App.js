import React, { Component } from 'react';
import axios from 'axios';
import Header from './components/Header';
import Main from './components/Main';
import Student from './components/Student';
import {Route, Switch}  from 'react-router-dom';
import Spinner from './components/Spinner';

class App extends Component {

  totalMarks=(marks)=>{
    let sum = 0;
    for (let i = 0; i < marks.length; i++) {
      sum += marks[i];
    }
    return sum;
  }

  constructor(props){
    super(props);

    this.state = {
      data:{},
      mainData:[],
      isBeingLoad:true,
      searchVal:"",
      sort:""
    };

    this.handleChange=this.handleChange.bind(this);
    this.handleClickAlpha=this.handleClickAlpha.bind(this);
    this.handleClickMarks=this.handleClickMarks.bind(this);
    this.handleClickRoll=this.handleClickRoll.bind(this);
  }

  componentDidMount(){

    axios.get("https://api.myjson.com/bins/1dlper")
    .then((res)=>{
      this.setState({
        data:res.data
      })
    }).then(()=>{
      let arr=(Object.keys(this.state.data));
      let dataArray=[];
      arr.forEach((key)=>{
        dataArray.push(this.state.data[key]);
      })
      this.setState({
        mainData:dataArray
      })
    }).then(()=>{
      let obj=this.state.mainData;
      obj.forEach((studentData,index)=>{
        let sum=this.totalMarks(Object.values(studentData.marks))
        obj[index].totalMarks=sum;
      })
      this.setState({
        mainData:obj
      })
    }).then(()=>{
      this.setState({
        isBeingLoad:false
      })
    }).catch((err)=>{
      console.error(err);
    })
  }

  handleChange(e){
    this.setState({
      searchVal:e.target.value
    })
  }

  handleClickAlpha(e){
    if(this.state.sort==="ALPHA_ASC")
      this.setState({
        sort: "ALPHA_DESC",
      })
    else
      this.setState({
        sort:"ALPHA_ASC",
      })
  }

  handleClickRoll(e){
    console.log(e.target.innerHTML);
    if(this.state.sort==="ROLL_ASC")
      this.setState({
        sort: "ROLL_DESC",
      })
    else
      this.setState({
        sort:"ROLL_ASC",
      })
  }

  handleClickMarks(e){
    console.log(e.target.innerHTML);
    if(this.state.sort==="TOTAL_ASC")
      this.setState({
        sort:"TOTAL_DESC",
      })
    else
      this.setState({
        sort:"TOTAL_ASC",
      })
  }

  render() {
    return (
      this.state.isBeingLoad?<Spinner />
      :
      <React-Fragment>
        <Header handleClickMarks={this.handleClickMarks} sort={this.state.sort} handleClickAlpha={this.handleClickAlpha} handleClickRoll={this.handleClickRoll} handleChange={this.handleChange} value={this.state.searchVal}/>
        <Switch>
          <Route path="/" exact strict render={()=>{
            return <Main sort={this.state.sort} search={this.state.searchVal} mainData={this.state.mainData}/>;
          }}/>
          <Route path="/:id" exact strict render={({match})=>{
            return <Student match={match} data={this.state.data}/>;
          }}/>
        </Switch>
      </React-Fragment>
    ); 
  }
}


export default App;
