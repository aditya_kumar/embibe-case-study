import React, { Component } from 'react';
import '../css/Main.css';
import {Link} from "react-router-dom"; 

class Main extends Component {

    constructor(props){
        super(props);

        this.sortBy=this.sortBy.bind(this);
    }

    //https://stackoverflow.com/questions/979256/sorting-an-array-of-javascript-objects
    //Logic used from this
    sortBy(field, reverse, primer){
        let key = primer ? 
            function(x) {return primer(x[field])} : 
            function(x) {return x[field]};
     
        reverse = !reverse ? 1 : -1;
     
        return function (a, b) {
            return (a = key(a), b = key(b), reverse * ((a > b) - (b > a)));
          } 
     }

  render() {
      let tempData=this.props.mainData;
      //Sorting  Logic
    switch(this.props.sort){
        case "TOTAL_ASC":tempData.sort(this.sortBy('totalMarks',false, parseInt));
                            break;
        case "TOTAL_DESC":tempData.sort(this.sortBy('totalMarks',true, parseInt));
                            break;
        case "ALPHA_ASC":tempData.sort(this.sortBy('name',false, function(a){return a.toUpperCase()}));
                            break;
        case "ALPHA_DESC":tempData.sort(this.sortBy('name',true, function(a){return a.toUpperCase()}));
                            break;
        case "ROLL_ASC": tempData.sort(this.sortBy('rollNo',false, parseInt));
                            break;
        case "ROLL_DESC": tempData.sort(this.sortBy('rollNo',true, parseInt));
                            break;                 
        default:break;
    }        
    return (
      <div className="Main">
       {
           tempData.map((studentData)=>{
               if(studentData.name.toLowerCase().startsWith(this.props.search.toLowerCase()))
               return (
                   <React-Fragment>
                        <Link to={"/"+studentData.rollNo}>
                            <div className="student">
                                <h3>Name: {studentData.name}</h3>
                                <h4>Roll No.: {studentData.rollNo}</h4>
                                <h3>Total Marks: {studentData.totalMarks} </h3>
                            </div>
                        </Link>
                   </React-Fragment>
               );
               else return null;
           })
       }
      </div>
    );
  }
}

export default Main;
