import React, { Component } from 'react';
import '../css/Student.css';
import {Link} from "react-router-dom";
import ReactHighcharts from 'react-highcharts'; // Expects that Highcharts was loaded in the code.

class Student extends Component {

    totalMarks=(marks)=>{
        let sum = 0;
        for (let i = 0; i < marks.length; i++) {
            sum += marks[i];
        }
        return sum;
    }

  render() {
    const student=this.props.data[Number(this.props.match.params.id)];
    const config ={
        chart: {
            type: 'column'
        },
        title: {
            text: 'Marks in all Subjects'
        },
        xAxis: {
            categories: Object.keys(student.marks)
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Marks'
            }
        },

        series: [{
            name: student.name,
            data: Object.values(student.marks)
    
        }]
    }

    return (
        <div className="Students">      
            <div className="Student">
                <h1>Name : {student.name} </h1>
                <h2>Roll Number: {student.rollNo}</h2>
                <h3>Marks: 
                    <ul>
                        {
                            Object.values(student.marks).map((mark,index)=>{
                                return (
                                    <li key={index}>{"Subject "+(index+1)} : {mark}</li>
                                );
                            })
                        }
                    </ul>
                </h3>
                <h2>Total Marks : {this.totalMarks(Object.values(student.marks))}</h2>
            </div>
            <div style={{marginTop:"100px"}}>
                <ReactHighcharts config = {config}></ReactHighcharts>
            </div>
            <div className="link" ><Link to="/">Go Back</Link></div>
        </div>
    );
  }
}

export default Student;