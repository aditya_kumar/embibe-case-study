import React, { Component } from 'react';
import '../css/Header.css';

class Header extends Component {

  render() {
    return (
      <div className="header">
        <input type="text" onChange={this.props.handleChange} value={this.props.searchVal} placeholder="&#x1f50d; Search Here"/>
        <button onClick={this.props.handleClickAlpha}>{this.props.sort==="ALPHA_ASC"?"Sort Reverse-Aplhabetical":"Sort Alphabetical"}</button>
        <button onClick={this.props.handleClickMarks}>{this.props.sort==="TOTAL_ASC"?"Sort by Descending Total Marks":"Sort by Ascending Total Marks"}</button>
        <button onClick={this.props.handleClickRoll}>{this.props.sort==="ROLL_ASC"?"Sort by Descending Roll":"Sort by Ascending Roll"}</button>
      </div>
    );
  }
}

export default Header;